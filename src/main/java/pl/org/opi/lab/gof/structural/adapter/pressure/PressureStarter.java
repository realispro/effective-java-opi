package pl.org.opi.lab.gof.structural.adapter.pressure;

import pl.org.opi.lab.gof.structural.adapter.pressure.bar.Bar2PascalPressureAdapter;
import pl.org.opi.lab.gof.structural.adapter.pressure.bar.BarPressureProvider;

public class PressureStarter {

    public static void main(String[] args) {
        System.out.println("let's measure a pressure !");


        PascalPressure pascalPressure = new Bar2PascalPressureAdapter(new BarPressureProvider());

        Display display = new Display();
        display.showPresure(pascalPressure);

    }
}
