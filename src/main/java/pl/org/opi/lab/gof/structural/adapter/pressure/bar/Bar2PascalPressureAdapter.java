package pl.org.opi.lab.gof.structural.adapter.pressure.bar;

import pl.org.opi.lab.gof.structural.adapter.pressure.PascalPressure;

public class Bar2PascalPressureAdapter implements PascalPressure {

    private BarPressureProvider barPressureProvider;

    public Bar2PascalPressureAdapter(BarPressureProvider barPressureProvider) {
        this.barPressureProvider = barPressureProvider;
    }

    @Override
    public float getPressure() {
        return barPressureProvider.getPressure() / 100_000;
    }
}
