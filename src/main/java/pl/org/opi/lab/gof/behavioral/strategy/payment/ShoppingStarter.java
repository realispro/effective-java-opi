package pl.org.opi.lab.gof.behavioral.strategy.payment;

public class ShoppingStarter {

    public static void main(String[] args) {
        System.out.println("let's buy smth!");

        Shopping s = new Shopping();

        s.addItem(new Item("mleko", 3, 2.2));
        s.addItem(new Item("Ipa", 24, 3.5));
        s.addItem(new Item("paluszki", 2, 1.79));

        s.pay( new Paypal2PaymentMethodAdapter(new Paypal("abc", "xyz"))
                //new CreditCard("65436123", 23)
                );

        //PaymentMethod pm = new Paypal("marcin", "secret");
        //PaymentMethod pm = PaymentMethodFactory.getPaymentMethod(PaymentType.CREDIT_CARD);
        //s.pay(pm);
    }
}