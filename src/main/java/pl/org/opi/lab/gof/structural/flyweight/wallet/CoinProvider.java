package pl.org.opi.lab.gof.structural.flyweight.wallet;

import java.util.HashMap;
import java.util.Map;

public class CoinProvider {

    private static Map<String, Coin> cache = new HashMap<>();

    public static Coin getCoin(int value, Currency currency) {
        String coinKey = value + currency.name();
        Coin coin = cache.get(coinKey);
        if (coin == null) {
            coin = new Coin(value, currency);
            cache.put(coinKey, coin);
        }
        return coin;
    }
}
