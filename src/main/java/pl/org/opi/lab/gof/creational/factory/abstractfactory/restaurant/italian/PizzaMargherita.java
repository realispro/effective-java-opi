package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.MainDish;

public class PizzaMargherita implements MainDish {
    @Override
    public void eat() {
        System.out.println("Eating PizzaMargherita");
    }
}
