package pl.org.opi.lab.gof.behavioral.templatemethod.encrypt;


public class EncryptStarter {

    public static void main(String[] args){
        SafeStore store = new AESStore();
        boolean stored = store.store("hide it!");
        System.out.println("safely stored? " + stored);
    }

}
