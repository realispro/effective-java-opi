package pl.org.opi.lab.gof.structural.proxy.net;

public interface Internet {


    /**
     * accessing to provided address according to internet implementation policy
     * @param address given address
     * @throws RuntimeException if policy violated
     */
    void connectTo(String address) throws RuntimeException;
}
