package pl.org.opi.lab.gof.structural.flyweight.particles;

import java.util.HashMap;
import java.util.Map;

public class ParticleCaliberPool {

    private static Map<Integer, ParticleCaliber> cache = new HashMap<>();

    public static ParticleCaliber getParticleCaliber(int caliber){

        ParticleCaliber particleCaliber = cache.get(caliber);
        if(particleCaliber==null){
            System.out.println("creating caliber " + caliber);
            particleCaliber = new ParticleCaliber(caliber);
            cache.put(caliber, particleCaliber);
        } else {
            System.out.println("reusing caliber " + caliber);
        }

        return particleCaliber;
    }


}
