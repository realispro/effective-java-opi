package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.polish;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.Dessert;

public class Sernik implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("Enjoying Sernik");
    }
}
