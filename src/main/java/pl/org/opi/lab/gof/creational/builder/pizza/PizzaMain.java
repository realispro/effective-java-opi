package pl.org.opi.lab.gof.creational.builder.pizza;

public class PizzaMain {

    public static void main(String[] args) {

        PizzaBuilder builder = new PizzaBuilder();

        Pizza pizza = builder.addDough("cienkie").addTopping("ser")
                .addTopping("pieczarki").addSause("keczup").bake();
        System.out.println("eating pizza " + pizza);


        Pizza pizza1 = new Pizza();
        pizza1.setSause("czosnkowy");

        pizza1.setDough("grube");
        pizza1.addTopping("salami");


    }

}
