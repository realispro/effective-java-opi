package pl.org.opi.lab.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CsvParser extends Parser{


    @Override
    protected String getFileExtension() {
        return ".csv";
    }

    @Override
    protected void parseFile(File f) throws IOException {
        Files
                .lines(Paths.get(f.getAbsolutePath()))
                .forEach(l -> {
                    String[] parsedLine = l.split(";");
                    for (String cell : parsedLine) {
                        System.out.print("[" + cell + "]");
                    }
                    System.out.println();
                });
    }
}
