package pl.org.opi.lab.gof.creational.singleton.sample;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class SingletonMain {


    public static void main(String[] args) {


        EnumSingleton s1 = EnumSingleton.getInstance();

        EnumSingleton s2 = null;

        // reflection API
        try {
            Class clazz = Class.forName("pl.org.opi.lab.gof.creational.singleton.sample.EnumSingleton");

            Constructor c = clazz.getDeclaredConstructor();
            c.setAccessible(true);

            s2 = (EnumSingleton) c.newInstance();


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //.LazySingleton.class;




        System.out.println("same?  " + (s1==s2));

    }
}
