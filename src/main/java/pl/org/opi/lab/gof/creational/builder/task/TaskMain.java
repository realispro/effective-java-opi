package pl.org.opi.lab.gof.creational.builder.task;

public class TaskMain {

    public static void main(String[] args) {

        TaskBuilder taskBuilder = new TaskBuilder();

        Task task = taskBuilder
                .setId(10)
                .setTitle("Task builder example")
                .setDescription("Create a task builder by yourself")
                .createTask();

        System.out.println();
        System.out.println("task = " + task);

        task.setDescription(task.getDescription() + ". Task completed.");
        task.setClosed(true);

        System.out.println();
        System.out.println("task = " + task);
    }
}
