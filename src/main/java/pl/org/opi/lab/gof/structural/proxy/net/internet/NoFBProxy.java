package pl.org.opi.lab.gof.structural.proxy.net.internet;


import pl.org.opi.lab.gof.structural.proxy.net.Internet;
import pl.org.opi.lab.gof.structural.proxy.net.internet.RealNetwork;

public class NoFBProxy implements Internet {

    private Internet internet = new RealNetwork();

    @Override
    public void connectTo(String address) throws RuntimeException {
        if (addressForbidded(address)) {
            throw new RuntimeException("Address [" + address + "] forbidden");
        }
        internet.connectTo(address);
    }

    private boolean addressForbidded(String address) {
        if (address.toLowerCase().contains("facebook")) {
            return true;
        }
        return false;
    }
}
