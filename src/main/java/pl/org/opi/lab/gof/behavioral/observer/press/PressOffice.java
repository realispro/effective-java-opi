package pl.org.opi.lab.gof.behavioral.observer.press;

import java.util.ArrayList;
import java.util.List;

public class PressOffice {

    private List<Subscriber> subscribers = new ArrayList<>();

    public void dayWork(){
        News n1 = new News("Mundial: Poland won!", "Polska mistrzem Polski");
        News n2 = new News("Chemitrails are fake", "Chemitrails doesn't exists");
        broadcast(n1);
        broadcast(n2);
    }

    private void broadcast(News news) {
        System.out.println("news: " + news.toString());
        for (Subscriber subscriber : subscribers) {
            subscriber.receive(news);
        }
    }
    public void subscribe(Subscriber subscriber) {
        System.out.println("new subscriber: " + subscriber);
        subscribers.add(subscriber);
    }
    public void unsubscribe(Subscriber subscriber) {
        System.out.println("unsubscribed: " + subscriber);
        subscribers.remove(subscriber);
    }



}
