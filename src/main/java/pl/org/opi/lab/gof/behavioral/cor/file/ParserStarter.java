package pl.org.opi.lab.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;

public class ParserStarter {

    public static void main(String[] args) throws IOException {
        System.out.println("let's parse a file");

        File file = new File("./src/main/resources/cor/cor.txt");
        Parser parser = getChain();
        parser.parse(file);
    }

    public static Parser getChain(){
        TxtParser txtParser = new TxtParser();
        CsvParser csvParser = new CsvParser();

        csvParser.setNext(txtParser);

        return csvParser;
    }




}
