package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo;

public interface Fish {

    void swim();

}
