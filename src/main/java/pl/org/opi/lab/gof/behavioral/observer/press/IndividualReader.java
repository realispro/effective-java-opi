package pl.org.opi.lab.gof.behavioral.observer.press;

public class IndividualReader implements Subscriber {

    private String name;

    public IndividualReader(String name) {
        this.name = name;
    }

    @Override
    public void receive(News news) {
        System.out.println(name + ": ");
        System.out.println("received: " + news.toString());
    }

    @Override
    public String toString() {
        return "IndividualReader{" +
                "name='" + name + '\'' +
                '}';
    }
}
