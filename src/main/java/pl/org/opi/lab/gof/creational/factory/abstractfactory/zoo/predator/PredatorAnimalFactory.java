package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.predator;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.AnimalFactory;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Bird;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Fish;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Mammal;

public class PredatorAnimalFactory implements AnimalFactory {
    @Override
    public Bird getBird() {
        return new Eagle();
    }

    @Override
    public Fish getFish() {
        return new Shark();
    }

    @Override
    public Mammal getMammal() {
        return new Tiger();
    }
}
