package pl.org.opi.lab.gof.structural.decorator.email;

public class EmailDecorator implements Email{

    private Email email;

    private String signature;

    public EmailDecorator(Email email, String signature) {
        this.email = email;
        this.signature = signature;
    }

    @Override
    public String getTitle() {
        return email.getTitle();
    }

    @Override
    public String getContent() {
        return email.getContent() + "\n\n" + getSignature();
    }

    public String getSignature(){
        return signature;
    }


}
