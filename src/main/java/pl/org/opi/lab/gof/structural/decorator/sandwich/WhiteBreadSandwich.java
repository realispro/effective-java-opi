package pl.org.opi.lab.gof.structural.decorator.sandwich;

public class WhiteBreadSandwich implements Sandwich{
    @Override
    public String content() {
        return "white bread";
    }
}
