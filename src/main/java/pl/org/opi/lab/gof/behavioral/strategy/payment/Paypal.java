package pl.org.opi.lab.gof.behavioral.strategy.payment;

public class Paypal {

    private String accountName;

    private String password;

    public Paypal() {
    }

    public Paypal(String accountName, String password) {
        this.accountName = accountName;
        this.password = password;
    }

    public void chargePaypal(double value) {
        System.out.println("connecting to paypal service, account name: " + accountName);
    }
}
