package pl.org.opi.lab.gof.structural.decorator.sandwich;

public class SandwichStarter {

    public static void main(String[] args) {
        System.out.println("let's eat some sandwich");

        Sandwich sandwich = new WhiteBreadSandwich();

        sandwich = new SandwichDecorator(sandwich, "cheese");
        sandwich = new SandwichDecorator(sandwich, "tomato");
        sandwich = new SandwichDecorator(sandwich, "spicy sauce");

        System.out.println("eating " + sandwich.content());
    }
}
