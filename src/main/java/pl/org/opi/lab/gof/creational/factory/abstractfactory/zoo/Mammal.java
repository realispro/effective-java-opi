package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo;

public interface Mammal {

    void move();
}
