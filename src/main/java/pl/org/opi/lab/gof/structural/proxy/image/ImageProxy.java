package pl.org.opi.lab.gof.structural.proxy.image;

public class ImageProxy implements Image{

    private String fileName;

    public ImageProxy(String fileName){
        this.fileName = fileName;
    }

    @Override
    public byte[] getData() {
        ImageFile image = new ImageFile(fileName);
        return image.getData();
    }
}
