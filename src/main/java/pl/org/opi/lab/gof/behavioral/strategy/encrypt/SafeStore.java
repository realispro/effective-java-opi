package pl.org.opi.lab.gof.behavioral.strategy.encrypt;


import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class SafeStore {


    public boolean store(Encrypter encrypter, Object o){
        try {
            byte[] toStore = encrypter.encrypt(o.toString());
            storeEncrypted(toStore);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private final void storeEncrypted(byte[] bytes){

        try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("target/safe.store"))){
            bos.write(bytes);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
