package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.polish;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.Dessert;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.MainDish;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.Restaurant;

public class ChlopskieJadlo implements Restaurant {
    @Override
    public MainDish getMainDish() {
        return new Flaki();
    }
    @Override
    public Dessert getDessert() {
        return new Sernik();
    }
}
