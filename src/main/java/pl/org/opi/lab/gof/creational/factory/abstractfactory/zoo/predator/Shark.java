package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.predator;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Fish;


public class Shark implements Fish {
    @Override
    public void swim() {
        System.out.println("shark is swimming dangerously");
    }
}
