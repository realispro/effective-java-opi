package pl.org.opi.lab.gof.creational.factory.method;

public class PilsBeer implements Beer {
    private String name;
    private float voltage;
    public PilsBeer(String name, float voltage) {
        this.name = name;
        this.voltage = voltage;
    }
    @Override
    public float getVoltage() {
        return voltage;
    }
    @Override
    public String getName() {
        return name;
    }
    @Override
    public void drinkBeer() {
        System.out.println("drinking tasty pils : " +name +", " + voltage + "%");
    }
    @Override
    public String toString() {
        return "PilsBeer{" +
                "name='" + name + '\'' +
                ", voltage=" + voltage +
                '}';
    }
}