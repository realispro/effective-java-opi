package pl.org.opi.lab.gof.behavioral.observer.press;

@FunctionalInterface
public interface Subscriber {

    void receive(News news);

}
