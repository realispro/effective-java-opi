package pl.org.opi.lab.gof.structural.flyweight.wallet;

import java.util.ArrayList;
import java.util.List;

public class WalletStarter {

    public static void main(String[] args) {
        System.out.println("let's put some coins into wallet");

        List<Coin> wallet = new ArrayList<>();
/*

        wallet.add(CoinProvider.getCoin(5, Currency.PLN));
        wallet.add(CoinProvider.getCoin(2, Currency.PLN));
        wallet.add(CoinProvider.getCoin(5, Currency.DOLLAR));
        wallet.add(CoinProvider.getCoin(1, Currency.EURO));
        wallet.add(CoinProvider.getCoin(5, Currency.PLN));
        wallet.add(CoinProvider.getCoin(1, Currency.EURO));

        System.out.println("wallet content: " + wallet);
*/

        wallet = new ArrayList<>();
        wallet.add(CoinPool.getCoin(5, Currency.PLN));
        wallet.add(CoinPool.getCoin(2, Currency.PLN));
        wallet.add(CoinPool.getCoin(5, Currency.DOLLAR));
        wallet.add(CoinPool.getCoin(1, Currency.EURO));
        wallet.add(CoinPool.getCoin(5, Currency.PLN));
        wallet.add(CoinPool.getCoin(1, Currency.EURO));
        System.out.println("wallet content: " + wallet);



    }
}
