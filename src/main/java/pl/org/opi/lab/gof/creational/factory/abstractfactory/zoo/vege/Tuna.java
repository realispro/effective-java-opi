package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.vege;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Fish;

public class Tuna implements Fish {
    @Override
    public void swim() {
        System.out.println("tuna is swimming fast");
    }
}
