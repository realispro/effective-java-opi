package pl.org.opi.lab.gof.creational.builder.pizza;

import java.util.ArrayList;
import java.util.List;

public class Pizza {

    private String dough;

    private List<String> toppings = new ArrayList<>();

    private String sause;

    public String getDough() {
        return dough;
    }

    public void setDough(String dough) {
        this.dough = dough;
    }

    public List<String> getToppings() {
        return toppings;
    }

    public void addTopping(String topping) {
        toppings.add(topping);
    }

    public String getSause() {
        return sause;
    }

    public void setSause(String sause) {
        this.sause = sause;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "dough='" + dough + '\'' +
                ", toppings=" + toppings +
                ", sause='" + sause + '\'' +
                '}';
    }
}
