package pl.org.opi.lab.gof.behavioral.strategy.encrypt;


public class EncryptStarter {

    public static void main(String[] args){
        SafeStore store = new SafeStore();
        Encrypter encrypter = new AESEncrypter();
        boolean stored = store.store(encrypter, "hide it!");
        System.out.println("safely stored? " + stored);
    }

}
