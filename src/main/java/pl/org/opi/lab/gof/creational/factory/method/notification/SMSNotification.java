package pl.org.opi.lab.gof.creational.factory.method.notification;

public class SMSNotification implements Notification{

    private String subject;

    private String description;

    public SMSNotification(String subject, String description) {
        this.subject = subject;
        this.description = description;
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void emit() {
        System.out.println("sending sms: " + subject + ", " + description);
    }

    @Override
    public String toString() {
        return "SMSNotification{" +
                "subject='" + subject + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
