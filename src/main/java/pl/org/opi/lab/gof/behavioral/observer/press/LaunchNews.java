package pl.org.opi.lab.gof.behavioral.observer.press;

public class LaunchNews {

    public static void main(String[] args) {

        PressOffice po = new PressOffice();

        IndividualReader individualReader1 = new IndividualReader("Jan Kowalski");
        IndividualReader individualReader2 = new IndividualReader("Piotr Nowak");

        po.subscribe(individualReader1);
        po.subscribe(individualReader2);
        po.subscribe(new AnotherPressOfficeSubscriber());
        po.subscribe(n -> System.out.println("ad hoc subscriber:" + n));

        po.dayWork();

        po.unsubscribe(individualReader1);

        po.dayWork();
    }
}
