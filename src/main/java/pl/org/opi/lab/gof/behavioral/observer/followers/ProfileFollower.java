package pl.org.opi.lab.gof.behavioral.observer.followers;

public class ProfileFollower implements Follower{

    private String name;

    public ProfileFollower(String name) {
        this.name = name;
    }

    @Override
    public void notify(Post p) {
        System.out.println("follower " + name + " notified that: " + p.getMessage());
    }
}
