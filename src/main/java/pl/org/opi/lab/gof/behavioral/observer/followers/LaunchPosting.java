package pl.org.opi.lab.gof.behavioral.observer.followers;

public class LaunchPosting {

    public static void main(String[] args) {

        Profile profile = new Profile();
        Profile profile2 = new Profile();

        Follower follower1 = new ProfileFollower("Johny");
        Follower follower2 = new ProfileFollower("Steve");
        Follower follower3 = new ProfileFollower("Agnes");

        profile.follow(follower1);
        profile.follow(follower2);

        profile2.follow(follower2);
        profile2.follow(follower3);

        profile.publish("totally unimportant message 1");
        profile2.publish("totally unimportant message 2");

        profile.unfollow(follower1);

        profile.publish("COVID-19 is fake!");
    }
}
