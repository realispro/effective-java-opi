package pl.org.opi.lab.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TxtParser extends Parser {

    @Override
    protected String getFileExtension() {
        return ".txt";
    }

    @Override
    protected void parseFile(File f) throws IOException {
        Files
                .lines(Paths.get(f.getAbsolutePath()))
                .forEach(System.out::println);
    }
}
