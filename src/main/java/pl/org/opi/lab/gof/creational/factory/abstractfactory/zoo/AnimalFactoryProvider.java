package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.predator.PredatorAnimalFactory;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.vege.VegeAnimalFactory;

public class AnimalFactoryProvider {


    public static AnimalFactory getFactory(AnimalType type){
        switch (type){
            case VEGE: return new VegeAnimalFactory();
            default: return new PredatorAnimalFactory();
        }
    }

}
