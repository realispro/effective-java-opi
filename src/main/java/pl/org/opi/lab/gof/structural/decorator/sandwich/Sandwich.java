package pl.org.opi.lab.gof.structural.decorator.sandwich;

public interface Sandwich {

    String content();
}
