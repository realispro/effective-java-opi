package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant;

public interface Appetizer {

    void taste();
}
