package pl.org.opi.lab.gof.structural.flyweight.wallet;

public enum Currency {

    PLN,
    EURO,
    DOLLAR
}
