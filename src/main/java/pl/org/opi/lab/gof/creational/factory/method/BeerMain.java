package pl.org.opi.lab.gof.creational.factory.method;

public class BeerMain {

    public static void main(String[] args) {
        BeerFactory beerFactory = new BeerFactory();
        Beer beer = BeerFactory.serveBeer("Lech",
                5.60F,
                BeerType.IPA);

        System.out.println("beer = " + beer);
        System.out.println("drinking beer = " + beer.getName() + ", voltage: " + beer.getVoltage());
        beer.drinkBeer();
    }

}
