package pl.org.opi.lab.gof.behavioral.mediator.chat;

/**
 * Created by xdzm on 2016-06-17.
 */
public class ChatPart {

    private String name;

    public ChatPart(String name) {
        this.name = name;
    }

    public void receive(String message){
        System.out.println("[" + name + "] someone send a letter: " + message);
    }
}
