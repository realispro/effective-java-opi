package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.vege;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Mammal;

public class Elephant implements Mammal {
    @Override
    public void move() {
        System.out.println("elephant is walking heavily");
    }
}
