package pl.org.opi.lab.gof.behavioral.cor.purchase;

public abstract class Approver {

    private String name;

    private int maxBudget;

    protected Approver next;

    public void setNext(Approver next) {
        this.next = next;
    }

    public Approver(String name, int maxBudget){
        this.name = name;
        this.maxBudget=maxBudget;
    }

    public void approve(Purchase p){
        if(p.getAmount()<maxBudget){
            System.out.println("Przyjęte do realizacji.");
            p.approve(name);
        } else {
            if(next!=null) {
                next.approve(p);
            }
        }
    }

    public String getName() {
        return name;
    }
}
