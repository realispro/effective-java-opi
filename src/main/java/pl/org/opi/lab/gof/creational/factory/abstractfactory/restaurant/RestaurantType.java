package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant;

public enum RestaurantType {

    POLSIH,
    ITALIAN
}
