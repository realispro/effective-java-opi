package pl.org.opi.lab.gof.creational.factory.method.notification;

public class NotificationMain {

    public static void main(String[] args) {

        NotificationFactory nf = new NotificationFactory();
        Notification notification = NotificationFactory.createNotification("urgent message",
                "urgent message description",
                NotificationType.MOBILE);

        System.out.println("notification = " + notification);
        notification.emit();

    }
}
