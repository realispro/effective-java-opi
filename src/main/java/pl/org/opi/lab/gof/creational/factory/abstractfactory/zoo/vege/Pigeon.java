package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.vege;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Bird;

public class Pigeon implements Bird {

    @Override
    public void fly() {
        System.out.println("pigeon is plying in a group");
    }
}
