package pl.org.opi.lab.gof.structural.adapter.pressure;

public class Display {

    public void showPresure(PascalPressure pp){
        System.out.println("current presure in Pascal: " + pp.getPressure());
    }
}
