package pl.org.opi.lab.gof.structural.decorator.email;


public interface Email {

    String getTitle();

    String getContent();
}
