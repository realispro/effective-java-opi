package pl.org.opi.lab.gof.creational.factory.method;

public class IpaBeer implements Beer {
    private String name;
    private float voltage;
    public IpaBeer(String name, float voltage) {
        this.name = name;
        this.voltage = voltage;
    }
    @Override
    public float getVoltage() {
        return voltage;
    }
    @Override
    public String getName() {
        return name;
    }
    @Override
    public void drinkBeer() {
        System.out.println("drinking ipa beer : " +name +", " + voltage + "%");
    }
    @Override
    public String toString() {
        return "IpaBeer{" +
                "name='" + name + '\'' +
                ", voltage=" + voltage +
                '}';
    }
}