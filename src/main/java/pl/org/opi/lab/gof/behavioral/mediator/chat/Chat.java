package pl.org.opi.lab.gof.behavioral.mediator.chat;

import java.util.ArrayList;
import java.util.List;

public class Chat {

    private List<ChatPart> parts = new ArrayList<>();


    public void join(ChatPart part){
        parts.add(part);
    }

    public void sendMessage(ChatPart part, String message){
        for(ChatPart p : parts){
            if(p!=part) {
                p.receive(message);
            }
        }
    }


}
