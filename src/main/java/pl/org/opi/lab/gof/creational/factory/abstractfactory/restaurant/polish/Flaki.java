package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.polish;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.MainDish;

public class Flaki implements MainDish {
    @Override
    public void eat() {
        System.out.println("Eating Flaki");
    }
}
