package pl.org.opi.lab.gof.structural.flyweight.particles;

//  flyweight must be IMMUTABLE
public class ParticleCaliber {

    int caliber;

    String icon;

    public ParticleCaliber(int caliber) {
        this.caliber = caliber;
        this.icon = caliber + ".jpg"; // loading image bytes
    }

    public int getCaliber() {
        return caliber;
    }

    public String getIcon() {
        return icon;
    }
}
