package pl.org.opi.lab.gof.creational.factory.method;

public interface Beer {

    float getVoltage();

    String getName();

    void drinkBeer();

}
