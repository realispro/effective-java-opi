package pl.org.opi.lab.gof.creational.factory.method;

public class FruityBeer implements Beer {
    private String name;
    private float voltage;

    public FruityBeer(String name, float voltage) {
        this.name = name;
        this.voltage = voltage;
    }

    @Override
    public float getVoltage() {
        return voltage;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void drinkBeer() {
        System.out.println("drinking fruity beer : " + name + ", " + voltage + "%");
    }

    @Override
    public String toString() {
        return "FruityBeer{" +
                "name='" + name + '\'' +
                ", voltage=" + voltage +
                '}';
    }
}
