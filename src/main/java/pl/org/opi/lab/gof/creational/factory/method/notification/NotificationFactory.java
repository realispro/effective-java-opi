package pl.org.opi.lab.gof.creational.factory.method.notification;

public class NotificationFactory {


    public static Notification createNotification(String subject, String description, NotificationType type) {

        switch (type) {
            case MOBILE:
                return new SMSNotification(subject, description);
            default:
                return new PigeonNotification(subject, description);
        }

    }

    public static Notification createNotification(String subject, String description) {
        return createNotification(subject, description, NotificationType.TRADITIONAL);
    }

}
