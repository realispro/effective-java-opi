package pl.org.opi.lab.gof.creational.builder.pizza;

public class PizzaBuilder {

    private Pizza pizza = new Pizza();


    public PizzaBuilder addDough(String dough){
        pizza.setDough(dough);
        return this;
    }

    public PizzaBuilder addTopping(String topping){
        if(pizza.getDough()==null){
            throw new IllegalStateException("define dough first!");
        }
        pizza.addTopping(topping);
        return this;
    }

    public PizzaBuilder addSause(String sause){
        pizza.setSause(sause);
        return this;
    }

    public Pizza bake(){
        if(pizza.getDough()==null){
            throw new IllegalStateException("define dough!");
        }
        return pizza;
    }

}
