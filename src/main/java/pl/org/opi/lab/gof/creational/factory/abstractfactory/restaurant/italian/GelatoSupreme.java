package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.Dessert;

public class GelatoSupreme implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("Enjoying GelatoSupreme");
    }
}
