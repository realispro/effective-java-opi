package pl.org.opi.lab.gof.creational.factory.method.notification;

public enum NotificationType {

    MOBILE,
    TRADITIONAL

}
