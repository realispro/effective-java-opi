package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.Dessert;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.MainDish;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.Restaurant;

public class MamaMia implements Restaurant {
    @Override
    public MainDish getMainDish() {
        return new PizzaMargherita();
    }
    @Override
    public Dessert getDessert() {
        return new GelatoSupreme();
    }
}
