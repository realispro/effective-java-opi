package pl.org.opi.lab.gof.behavioral.strategy.payment;

public class Paypal2PaymentMethodAdapter implements PaymentMethod {

    private Paypal paypal;

    public Paypal2PaymentMethodAdapter(Paypal paypal) {
        this.paypal = paypal;
    }


    @Override
    public boolean charge(double value) {
        paypal.chargePaypal(value);
        return true;
    }
}
