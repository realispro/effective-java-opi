package pl.org.opi.lab.gof.structural.flyweight.particles;

public class GameStarter {

    public static void main(String[] args) {

        Unit u1 = new Unit("u1", 5,2);
        Unit u2 = new Unit("u2",4,2);
        Unit u3 = new Unit("u3",4,2);

        u1.fire(u2);
        u2.fire(u3);
        u3.fire(u2);
        u2.fire(u1);
        u1.fire(u2);
        u2.fire(u3);
        u3.fire(u2);
        u2.fire(u1);
    }
}
