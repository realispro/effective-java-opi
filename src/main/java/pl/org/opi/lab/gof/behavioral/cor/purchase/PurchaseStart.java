package pl.org.opi.lab.gof.behavioral.cor.purchase;

public class PurchaseStart {

    public static void main(String[] args) {
        System.out.println("let's process a purchase");

        Purchase p = new Purchase(24162361, "samochod dla sprzedawcy");
        Purchase p2 = new Purchase(5, "spinacze");
        Purchase p3 = new Purchase(3000, "komputer");
        Purchase p4= new Purchase(70_000_000, "wybory");

        Approver approver = getApprover();
        approver.approve(p);
        approver.approve(p2);
        approver.approve(p3);
        approver.approve(p4);

        System.out.println("p state: " + p);
        System.out.println("p2 state: " + p2);
        System.out.println("p3 state: " + p3);
        System.out.println("p4 state: " + p4);
    }

    private static Approver getApprover(){
        President president =new President("prezydent",50_000_000);
        Director director = new Director("inz. Karwowski",6000);
        Secretary secretary= new Secretary("sekretarka",50);

        secretary.setNext(director);
        director.setNext(president);
        return secretary;
    }
}
