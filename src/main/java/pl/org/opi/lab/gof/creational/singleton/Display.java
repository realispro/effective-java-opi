package pl.org.opi.lab.gof.creational.singleton;

public class Display {


    private static Display instance;

    private Display() {
    }

    public static Display getInstance() {
        if (instance == null) {
            synchronized (Display.class) {
                if (instance == null) {
                    instance = new Display();
                }
            }
        }
        return instance;
    }

    public void show(String text) {
        System.out.println("[" + text + "]");
    }

}
