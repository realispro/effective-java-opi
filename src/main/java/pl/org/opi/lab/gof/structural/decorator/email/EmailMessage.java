package pl.org.opi.lab.gof.structural.decorator.email;


public class EmailMessage implements Email {

    private String title;

    private String content;

    public EmailMessage(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
