package pl.org.opi.lab.gof.structural.decorator.email;

public class EmailStarter {


    public static void main(String[] args) {
        System.out.println("let's send some email");

        Email email = new EmailMessage("decorator pattern lesson", "learning decorator pattern");

        email = new EmailDecorator(email, "Cheers,\nJoe");
        email = new EmailDecorator(email, "RODO CLAUSE");

        System.out.println("sending email: title=[" + email.getTitle() + "], content=[\n" + email.getContent() + "\n]");

    }

}
