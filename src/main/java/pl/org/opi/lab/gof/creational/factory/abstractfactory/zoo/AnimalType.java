package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo;

public enum AnimalType {

    VEGE,
    PREDATOR
}
