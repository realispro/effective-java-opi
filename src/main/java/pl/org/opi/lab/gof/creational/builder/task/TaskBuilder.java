package pl.org.opi.lab.gof.creational.builder.task;

public class TaskBuilder {

    private Task task = new Task();

    public TaskBuilder setId(int taskId) {
        task.setId(taskId);
        return this;
    }

    public TaskBuilder setDescription(String description) {
        if (task.getTitle() == null) {
            throw new IllegalStateException("define task Title first!");
        }
        task.setDescription(description);
        return this;
    }

    public TaskBuilder setTitle(String title) {
        if (task.getId() == 0) {
            throw new IllegalStateException("define task ID first!");
        }
        task.setTitle(title);
        return this;
    }

    public TaskBuilder setPriority(int priority) {
        task.setId(priority);
        return this;
    }

    public TaskBuilder setClosed(boolean isClosed) {
        task.setClosed(isClosed);
        return this;
    }

    public Task createTask() {
        if (task.getId() == 0) {
            throw new IllegalStateException("define task ID!");
        } else if (task.getTitle()==null) {
            throw new IllegalStateException("define task title!");
        } else if (task.getDescription()==null) {
            throw new IllegalStateException("define task description!");
        }
        return task;
    }
}
