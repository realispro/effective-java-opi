package pl.org.opi.lab.gof.behavioral.templatemethod.content;

public class ContentStarter {


    public static void main(String[] args) {
        System.out.println("let's represent some content");

        ContentRepresentation content = new ContentRepresentation("some content");
        System.out.println(content.representContent());
        // <content>text</content>
        // { content: text }

    }
}
