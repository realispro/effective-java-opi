package pl.org.opi.lab.gof.structural.proxy.net;


import pl.org.opi.lab.gof.structural.proxy.net.internet.NoFBProxy;

public class InternetStarter {

    public static void main(String[] args) {
        System.out.println("lets surf!");

        Internet internet = new NoFBProxy();

        internet.connectTo("wp.pl");
        internet.connectTo("facebook.com/events");

    }
}
