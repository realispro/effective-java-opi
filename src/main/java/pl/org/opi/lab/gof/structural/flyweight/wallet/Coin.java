package pl.org.opi.lab.gof.structural.flyweight.wallet;

// flyweight candidate
public class Coin {

    private int value;

    private Currency currency;

    public Coin(int value, Currency currency) {
        this.value = value;
        this.currency = currency;
    }

    public int getValue() {
        return value;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "Coin{" +
                "value=" + value +
                ", currency=" + currency +
                '}';
    }
}
