package pl.org.opi.lab.gof.creational.singleton;

public class DisplayMain {

    public static void main(String[] args) {

        // TODO d1 and d2 as singleton

        Display d1 = Display.getInstance();
        Display d2 = Display.getInstance();

        d1.show("text from d1");
        d2.show("text from d2");
        System.out.println("same? " + (d1==d2));
    }
}
