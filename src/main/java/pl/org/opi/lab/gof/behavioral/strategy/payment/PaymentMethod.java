package pl.org.opi.lab.gof.behavioral.strategy.payment;

public interface PaymentMethod {

    boolean charge(double value);

}
