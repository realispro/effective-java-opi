package pl.org.opi.lab.gof.structural.proxy.image;

public interface Image {

    byte[] getData();

}
