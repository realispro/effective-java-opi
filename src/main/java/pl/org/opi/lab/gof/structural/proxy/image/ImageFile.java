package pl.org.opi.lab.gof.structural.proxy.image;

public class ImageFile implements Image{

    private byte[] data;

    public ImageFile(String fileName){
        // loading image data
        System.out.println("loading data from file " + fileName);
        data = fileName.getBytes(); // data loading emulation
    }


    @Override
    public byte[] getData() {
        return data;
    }
}
