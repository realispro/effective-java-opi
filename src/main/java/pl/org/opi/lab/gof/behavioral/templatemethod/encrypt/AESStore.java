package pl.org.opi.lab.gof.behavioral.templatemethod.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public class AESStore extends CipherStore{

    private byte[] keyValue = new byte[]{
            '9', '2', '3', '4', '5', '6', '7', '8',
            '1', '2', '3', '4', '5', '6', '7', '8'
    };


    @Override
    protected byte[] getKey() {
        return keyValue;
    }

    @Override
    protected String getAlgorithm() {
        return "AES";
    }
}
