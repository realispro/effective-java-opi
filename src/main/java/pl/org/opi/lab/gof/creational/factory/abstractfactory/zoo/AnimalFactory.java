package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo;

public interface AnimalFactory {

    Bird getBird();

    Fish getFish();

    Mammal getMammal();

}
