package pl.org.opi.lab.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;

public abstract class Parser {

    protected Parser next;

    public void setNext(Parser next) {
        this.next = next;
    }

    public final void parse(File f) throws IOException{

        if(f.getName().endsWith(getFileExtension())){
            parseFile(f);
        } else {
            if (next != null) {
                next.parse(f);
            }
        }

    }

    protected abstract String getFileExtension();

    protected abstract void parseFile(File f) throws IOException;
}
