package pl.org.opi.lab.gof.behavioral.mediator.queue;

public class Envelope {

    private String message;

    public Envelope(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Envelope{" +
                "message='" + message + '\'' +
                '}';
    }
}
