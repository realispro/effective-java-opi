package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.vege;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.AnimalFactory;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Bird;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Fish;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Mammal;

public class VegeAnimalFactory implements AnimalFactory {

    @Override
    public Bird getBird() {
        return new Pigeon();
    }

    @Override
    public Fish getFish() {
        return new Tuna();
    }

    @Override
    public Mammal getMammal() {
        return new Elephant();
    }
}
