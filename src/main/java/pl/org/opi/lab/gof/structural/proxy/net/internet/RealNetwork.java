package pl.org.opi.lab.gof.structural.proxy.net.internet;


import pl.org.opi.lab.gof.structural.proxy.net.Internet;

public class RealNetwork implements Internet {

    RealNetwork(){}

    @Override
    public void connectTo(String address) {
        System.out.println("connecting to " + address);
    }
}
