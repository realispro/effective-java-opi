package pl.org.opi.lab.gof.creational.factory.method.notification;

public interface Notification {

    String getSubject();

    String getDescription();

    void emit();

}
