package pl.org.opi.lab.gof.behavioral.observer.followers;

public interface Follower {

    void notify(Post p);

}
