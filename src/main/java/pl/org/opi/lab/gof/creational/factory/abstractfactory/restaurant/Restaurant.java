package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant;

public interface Restaurant {

    MainDish getMainDish();

    Dessert getDessert();
}
