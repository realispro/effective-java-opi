package pl.org.opi.lab.gof.structural.adapter.sim;

public class Mini2SimAdapter implements Sim {

    private MiniSimCard miniSimCard;

    public Mini2SimAdapter(MiniSimCard miniSimCard) {
        this.miniSimCard = miniSimCard;
    }

    public String getId() {
        return new String(miniSimCard.getId());
    }
}