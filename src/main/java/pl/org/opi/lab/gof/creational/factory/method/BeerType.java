package pl.org.opi.lab.gof.creational.factory.method;

public enum BeerType {
    PILS,
    IPA,
    FRUITY
}
