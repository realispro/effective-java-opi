package pl.org.opi.lab.gof.behavioral.observer.followers;

import java.util.ArrayList;
import java.util.List;

public class Profile {

    private List<Follower> followers = new ArrayList<>();

    public void publish(String message) {
        Post p = new Post(message);
        System.out.println("post generated: " + p.getMessage());
        for(Follower follower : followers){
            follower.notify(p);
        }
    }

    public void follow(Follower follower){
        followers.add(follower);
    }

    public void unfollow(Follower follower){
        followers.remove(follower);
    }



}
