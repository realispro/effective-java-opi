package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant;

public interface Dessert {

    void enjoy();
}
