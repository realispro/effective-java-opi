package pl.org.opi.lab.gof.creational.singleton.sample;

public enum EnumSingleton {

    INSTANCE;

    int field = 123;

    public static EnumSingleton getInstance(){
        return INSTANCE;
    }

    public String getDescription(){
        return "enum singleton " + field;
    }

}
