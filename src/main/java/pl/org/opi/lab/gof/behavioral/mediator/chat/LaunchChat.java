package pl.org.opi.lab.gof.behavioral.mediator.chat;

/**
 * Created by xdzm on 2016-06-17.
 */
public class LaunchChat {

    public static void main(String[] args) {
        ChatPart cp1 = new ChatPart("Janek");
        ChatPart cp2 = new ChatPart(" Kasia");
        ChatPart cp3 = new ChatPart("Agnieszka");

        Chat chat = new Chat();
        chat.join(cp1);
        chat.join(cp2);
        chat.join(cp3);

        chat.sendMessage(cp1, "Hey Everyone");
        chat.sendMessage(cp2, "Hejka!");


    }
}
