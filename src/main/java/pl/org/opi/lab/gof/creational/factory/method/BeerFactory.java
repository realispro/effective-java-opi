package pl.org.opi.lab.gof.creational.factory.method;

public class BeerFactory {

    public static Beer serveBeer(String name, float voltage, BeerType type) {
        switch (type) {
            case PILS:
                return new PilsBeer(name, voltage);
            case IPA:
                return new IpaBeer(name, voltage);
            case FRUITY:
                return new FruityBeer(name, voltage);
            default:
                return new PilsBeer(name, voltage);
        }
    }
    public static Beer serveBeer(String name, float voltage) {
        return serveBeer(name, voltage, BeerType.PILS);
    }
}