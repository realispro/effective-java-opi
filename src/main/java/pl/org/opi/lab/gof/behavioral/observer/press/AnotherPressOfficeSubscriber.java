package pl.org.opi.lab.gof.behavioral.observer.press;

public class AnotherPressOfficeSubscriber implements Subscriber {

    @Override
    public void receive(News news) {
        System.out.println("another press office: " + news);
    }
}
