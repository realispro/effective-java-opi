package pl.org.opi.lab.gof.behavioral.strategy.payment;

public class CreditCard implements PaymentMethod {


    private String number;

    private int ccv;


    public CreditCard() {
    }

    public CreditCard(String number, int ccv) {
        this.number = number;
        this.ccv = ccv;
    }

    @Override
    public boolean charge(double value) {
        System.out.println("charging card " + number);
        return true;
    }
}
