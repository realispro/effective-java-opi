package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.predator.PredatorAnimalFactory;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.vege.VegeAnimalFactory;

public class ZooMain {

    public static void main(String[] args) {

        AnimalFactory factory = AnimalFactoryProvider.getFactory(AnimalType.VEGE);

        Bird bird = factory.getBird();
        Fish fish = factory.getFish();
        Mammal mammal = factory.getMammal();

        bird.fly();
        fish.swim();
        mammal.move();



    }

}
