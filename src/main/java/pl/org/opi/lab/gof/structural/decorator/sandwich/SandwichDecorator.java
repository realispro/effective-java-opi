package pl.org.opi.lab.gof.structural.decorator.sandwich;

public class SandwichDecorator implements Sandwich {

    private Sandwich sandwich;
    private String part;

    public SandwichDecorator(Sandwich sandwich, String part) {
        this.sandwich = sandwich;
        this.part = part;
    }

    @Override
    public String content() {
        return sandwich.content() + " with " + part;
    }

}
