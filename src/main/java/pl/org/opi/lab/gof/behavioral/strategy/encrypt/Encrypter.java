package pl.org.opi.lab.gof.behavioral.strategy.encrypt;

public interface Encrypter {

    byte[] encrypt(String s) throws Exception;

}
