package pl.org.opi.lab.gof.structural.adapter.pressure;

public interface PascalPressure {

    /**
     * provides pressure information
     * @return value in pascals
     */
    float getPressure();
}
