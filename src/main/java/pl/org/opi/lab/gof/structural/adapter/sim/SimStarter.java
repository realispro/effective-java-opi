package pl.org.opi.lab.gof.structural.adapter.sim;

public class SimStarter {

    public static void main(String[] args) {
        System.out.println("let's install sim card");

        Device d = new Device();
        MiniSimCard miniSimCard = new MiniSimCard("1234");

        Sim sim = new Mini2SimAdapter(miniSimCard);
        d.install(sim);

    }
}
