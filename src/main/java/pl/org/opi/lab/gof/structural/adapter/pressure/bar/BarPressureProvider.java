package pl.org.opi.lab.gof.structural.adapter.pressure.bar;

import java.util.Random;

public class BarPressureProvider {

    public float getPressure(){
        return new Random().nextFloat();
    }

}
