package pl.org.opi.lab.gof.creational.factory.method.notification;

public class PigeonNotification implements Notification{

    private String subject;

    private String description;

    public PigeonNotification(String subject, String description) {
        this.subject = subject;
        this.description = description;
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void emit() {
        System.out.println("pigeon is flying: " + subject + ", " + description);
    }

    @Override
    public String toString() {
        return "PigeonNotification{" +
                "subject='" + subject + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
