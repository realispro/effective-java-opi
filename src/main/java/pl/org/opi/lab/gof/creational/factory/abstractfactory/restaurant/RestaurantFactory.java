package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.italian.MamaMia;
import pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant.polish.ChlopskieJadlo;

public class RestaurantFactory {
    public static Restaurant getRestaurant(RestaurantType t) {
        switch(t) {
            case ITALIAN:
                return new MamaMia();
            default:
                return new ChlopskieJadlo();
        }
    }
}
