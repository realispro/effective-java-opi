package pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.predator;

import pl.org.opi.lab.gof.creational.factory.abstractfactory.zoo.Mammal;

public class Tiger implements Mammal {
    @Override
    public void move() {
        System.out.println("tiger is running quietly");
    }
}
