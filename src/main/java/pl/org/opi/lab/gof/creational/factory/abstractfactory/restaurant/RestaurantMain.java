package pl.org.opi.lab.gof.creational.factory.abstractfactory.restaurant;

public class RestaurantMain {

    public static void main(String[] args) {

        Restaurant restaurant = RestaurantFactory.getRestaurant(RestaurantType.ITALIAN);

        MainDish mainDish = restaurant.getMainDish();
        mainDish.eat();

        Dessert dessert = restaurant.getDessert();
        dessert.enjoy();

        restaurant = RestaurantFactory.getRestaurant(RestaurantType.POLSIH);
        mainDish = restaurant.getMainDish();
        mainDish.eat();

        dessert = restaurant.getDessert();
        dessert.enjoy();
    }
}
