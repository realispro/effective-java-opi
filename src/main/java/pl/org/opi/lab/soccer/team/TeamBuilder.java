package pl.org.opi.lab.soccer.team;

import java.util.List;

public class TeamBuilder {

    private Team team = new Team();

    public TeamBuilder setName(String name){
        team.setName(name);
        return this;
    }

    public TeamBuilder setGoalKeeper(Player p){
        team.setGoalKeeper(p);
        return this;
    }

    public TeamBuilder setDefeners(List<Player> players){
        team.setDefenders(players);
        return this;
    }

    public TeamBuilder setMidfields(List<Player> players){
        team.setMidfields(players);
        return this;
    }

    public TeamBuilder setAttackers(List<Player> players){
        team.setAttackers(players);
        return this;
    }

    public Team construct(){
        return team;
    }


}
