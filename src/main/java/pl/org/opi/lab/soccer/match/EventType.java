package pl.org.opi.lab.soccer.match;

public enum EventType {
    GOAL,
    YELLOW_CARD,
    RED_CARD
}