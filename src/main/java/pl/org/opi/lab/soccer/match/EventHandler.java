package pl.org.opi.lab.soccer.match;

public abstract class EventHandler {

    protected EventHandler next;

    public void handleEvent(Event e){
        if(next!=null){
            next.handleEvent(e);
        }
    }
}
