package pl.org.opi.lab.soccer.team;

import pl.org.opi.lab.soccer.team.players.PlayersPicker;
import pl.org.opi.lab.soccer.team.players.PlayersPool;

public class Coach {

    private String name;

    private PlayersPool pool;

    public Coach(String name, PlayersPool pool) {
        this.name = name;
        this.pool = pool;
    }

    public Team constructTeam(){

        PlayersPicker picker = new PlayersPicker(pool);
        TeamBuilder builder = new TeamBuilder();

        return builder.setName(name)
                .setGoalKeeper(picker.getGoalKeeper())
                .setDefeners(picker.getDefenders(4))
                .setMidfields(picker.getMidfields(4))
                .setAttackers(picker.getAttackers(2))
                .construct();
    }
}
