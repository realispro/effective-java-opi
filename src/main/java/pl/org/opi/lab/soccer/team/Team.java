package pl.org.opi.lab.soccer.team;

import java.util.List;
import java.util.Objects;

public class Team {

    private String name;

    private Player goalKeeper;

    private List<Player> defenders;

    private List<Player> midfields;

    private List<Player> attackers;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player getGoalKeeper() {
        return goalKeeper;
    }

    public void setGoalKeeper(Player goalKeeper) {
        this.goalKeeper = goalKeeper;
    }

    public List<Player> getDefenders() {
        return defenders;
    }

    public void setDefenders(List<Player> defenders) {
        this.defenders = defenders;
    }

    public List<Player> getMidfields() {
        return midfields;
    }

    public void setMidfields(List<Player> midfields) {
        this.midfields = midfields;
    }

    public List<Player> getAttackers() {
        return attackers;
    }

    public void setAttackers(List<Player> attackers) {
        this.attackers = attackers;
    }

    public String info(){
        return toString();
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                "\ngoalKeeper=" + goalKeeper +
                "\ndefenders=" + defenders +
                "\nmidfields=" + midfields +
                "\nattackers=" + attackers +
                "}\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(name, team.name) &&
                Objects.equals(goalKeeper, team.goalKeeper) &&
                Objects.equals(defenders, team.defenders) &&
                Objects.equals(midfields, team.midfields) &&
                Objects.equals(attackers, team.attackers);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, goalKeeper, defenders, midfields, attackers);
    }
}
