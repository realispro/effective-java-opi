package pl.org.opi.lab.soccer.match.handlers;

import pl.org.opi.lab.soccer.match.Event;
import pl.org.opi.lab.soccer.match.EventHandler;
import pl.org.opi.lab.soccer.match.EventType;
import pl.org.opi.lab.soccer.match.Match;
import pl.org.opi.lab.soccer.team.Player;
import pl.org.opi.lab.soccer.team.Team;

public class GoalHandler extends EventHandler {

    @Override
    public void handleEvent(Event e) {

        if(e.type== EventType.GOAL){
            Match m = e.match;
            Team t = e.team;
            m.score(t);
            System.out.println("goal for " + t.getName() + " by " + e.player.toString());
        } else {
            super.handleEvent(e);
        }
    }
}
