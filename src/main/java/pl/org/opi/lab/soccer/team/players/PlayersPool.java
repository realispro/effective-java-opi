package pl.org.opi.lab.soccer.team.players;


import pl.org.opi.lab.soccer.team.Player;

import java.util.List;

public interface PlayersPool {

    List<Player> getGoalKeepers();

    List<Player> getDefenders();

    List<Player> getMidfields();

    List<Player> getAttackers();


}
