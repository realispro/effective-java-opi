package pl.org.opi.lab.soccer.match.handlers;

import pl.org.opi.lab.soccer.match.Event;
import pl.org.opi.lab.soccer.match.EventHandler;
import pl.org.opi.lab.soccer.match.EventType;
import pl.org.opi.lab.soccer.match.Match;
import pl.org.opi.lab.soccer.team.Player;
import pl.org.opi.lab.soccer.team.Team;

public class RedCardHandler extends EventHandler {

    @Override
    public void handleEvent(Event e) {

        if(e.type== EventType.RED_CARD){
            Team t = e.team;
            Player p = e.player;
            t.getDefenders().remove(p);
            t.getMidfields().remove(p);
            t.getAttackers().remove(p);

            System.out.println("red card for " + e.player.toString());
        } else {
            super.handleEvent(e);
        }
    }

}
