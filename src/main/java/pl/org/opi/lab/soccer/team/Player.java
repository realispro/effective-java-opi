package pl.org.opi.lab.soccer.team;

public class Player {

    private int rank;

    private String firstName;

    private String lastName;

    public Player(String firstName, String lastName, int rank) {
        this.rank = rank;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getRank() {
        return rank;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "{" + lastName + ':' + firstName + ':' + rank + "}";
    }
}
