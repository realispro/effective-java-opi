package pl.org.opi.lab.soccer.team.players;

import pl.org.opi.lab.soccer.team.Player;

import java.util.List;
import java.util.stream.Collectors;


public class PlayersPicker {

    private PlayersPool players;

    public PlayersPicker(PlayersPool players) {
        this.players = players;
    }

    public Player getGoalKeeper() {
        return findByRank(
                players.getGoalKeepers(), 1)
                .stream().findFirst()
                .orElseThrow(() -> new IllegalStateException("no goalkeeper"));
    }

    public List<Player> getDefenders(int amount) {
        return findByRank(players.getDefenders(), amount);
    }

    public List<Player> getMidfields(int amount) {
        return findByRank(players.getMidfields(), amount);
    }

    public List<Player> getAttackers(int amount) {
        return findByRank(players.getAttackers(), amount);
    }

    private List<Player> findByRank(List<Player> players, int amount) {
        return players.stream()
                .sorted((p1, p2) -> p1.getRank() - p2.getRank())
                .limit(amount)
                .collect(Collectors.toList());
    }
}
