package pl.org.opi.lab.soccer;


import pl.org.opi.lab.soccer.match.Match;
import pl.org.opi.lab.soccer.team.Coach;
import pl.org.opi.lab.soccer.team.Team;
import pl.org.opi.lab.soccer.team.players.GermanPlayersPool;
import pl.org.opi.lab.soccer.team.players.PolishPlayersPool;

public class MatchStarter {

    public static void main(String[] args) {

        Coach brzeczek = new Coach("Polska", PolishPlayersPool.getInstance());
        Coach loew = new Coach("Germany", GermanPlayersPool.getInstance());

        Team polska = brzeczek.constructTeam();
        Team niemcy = loew.constructTeam();

        Match match = new Match(polska, niemcy);

        match.introduction();
        match.play();
        match.showResult();
    }
}
