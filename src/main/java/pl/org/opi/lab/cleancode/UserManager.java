package pl.org.opi.lab.cleancode;

public class UserManager { // UpperCamelCase = PascalCase

    public static final int ID = 1;

    // order: public, protected, default/package, private
    protected String description = "description";

    private String serviceName = "UserManager"; // lowerCamelCase

    public String createUser(String firstName, String lastName, String role,
                             String address) {
        // TODO implement user creation

        return "sample user";
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
