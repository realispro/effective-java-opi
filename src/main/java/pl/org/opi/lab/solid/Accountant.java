package pl.org.opi.lab.solid;

public class Accountant extends Employee{


    public Accountant(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }

    public void doAccounting(){
        System.out.println("perform accounting...");
    }

}
