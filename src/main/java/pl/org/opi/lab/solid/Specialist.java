package pl.org.opi.lab.solid;


public class Specialist extends Employee implements IncidentHandler {

    public Specialist(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }

    public void doMagic(){
        System.out.println("doing specific magic");
    }

    @Override
    public void handleIncident(String incident, IncidentHandler support){
        System.out.println("handling incident: " + incident);
    }

}
