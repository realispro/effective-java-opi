package pl.org.opi.lab.solid;

import java.util.Random;

public class Manager extends Employee implements Management{


    public Manager(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }

    public void performAssessment(Employee employee){
        int rate = new Random().nextInt(10) + 1;
        if(rate>=7){
            employee.setGrade(employee.getGrade()+1);
        }
    }

}
