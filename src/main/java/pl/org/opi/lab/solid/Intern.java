package pl.org.opi.lab.solid;

public class Intern implements IncidentHandler {
    @Override
    public void handleIncident(String incident, IncidentHandler support) {
        System.out.println("intern is handling incident: " + incident);
    }
}
