package pl.org.opi.lab.solid;

@FunctionalInterface
public interface IncidentHandler {
    void handleIncident(String incident, IncidentHandler support);
}
