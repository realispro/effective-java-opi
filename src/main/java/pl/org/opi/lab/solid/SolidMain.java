package pl.org.opi.lab.solid;

public class SolidMain {

    public static void main(String[] args) { // psvm
        System.out.println("SolidMain.main"); // soutm

        Director director = new Director("Adam", "Nowak");

        Manager manager = new Manager("Jan", "Kowalski", 8);

        Specialist specialist = new Specialist("Joe", "Doe", 5);
        specialist.doMagic();

        Accountant accountant = new Accountant("John", "Smith", 3);
        accountant.doAccounting();


        String announcement = "annual assessment time!";
        Management management = manager;

        management.sendAnnouncement(announcement);

        Intern intern = new Intern();
        Intern intern1 = new Intern();
        String incident = "Critical issue!!!";
        IncidentHandler incidentHandler = intern;
        incidentHandler.handleIncident(incident, intern1);


        manager.performAssessment(specialist);
        manager.performAssessment(accountant);

        System.out.println("specialist=" + specialist);
        System.out.println("accountant=" + accountant);

    }

}
