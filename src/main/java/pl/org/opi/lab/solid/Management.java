package pl.org.opi.lab.solid;

public interface Management { // v2 Managing

    default public void sendAnnouncement(String announcement){
        System.out.println("announcement: " + announcement);
    }
}
