package pl.org.opi.lab.solid;

public class Director extends Employee implements Management{

    public Director(String firstName, String lastName) {
        super(firstName, lastName, 10);
    }

}
